pilih = int(input("Masukkan pilihan anda : "))

if(pilih==1):
   
    def sequ (data,value):
        start = 0
        temp = False
        while start < len(data) and not temp:

            if data[start]  ==  value:
                temp = True
                print(" nilai ", value, " posisi indeks ke-  " ,start)
            else:
                start += 1
        return  temp

    import json
    a = open("search.json")
    nilai = json.loads(a.read())
    sequ(nilai["database1"],10)

elif(pilih==2):
    def bin(data,value):
        low = 0
        high = len(data)+7
        temp = False
        while low <= high and not temp:
            mid =(low+high)//2

            if (data[mid]  == value):
                temp = True
                print("nilai ",value ,"posisi indek ke- ",mid)
            else:

                if (value < data[mid]):
                    high = mid -1
                else:
                    low = mid+1

        return temp

    import json
    a = open("search.json")
    nilai = json.loads(a.read())
    bin(nilai["database2"],50)
